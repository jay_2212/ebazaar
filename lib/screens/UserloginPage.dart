import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shopping_cart/model/validation.dart';
import 'package:shopping_cart/screens/ShopRegistration.dart';
import 'package:shopping_cart/screens/UserSignupPage.dart';
import 'package:shopping_cart/ui/homepage.dart';

class LoginScreen extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
       // primarySwatch: Colors.white,
      ),
      home: LoginPage(),
    );
  }
}


String token;
class LoginPage extends StatefulWidget {
  // static String tag = 'login-page';
  LoginPage({ Key key, String title }) : super(key: key);
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  bool _loading = false;
  //  LoginRequestData _loginData = LoginRequestData();
  bool _obscureText = true;

  String _email;
  String _password;

  String errorMsg;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          title: Center(child: (Text("Login Page",style: TextStyle(color: Colors.white))),
          )),
      body: new Center(
        child: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }
  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        new SizedBox(height: 50.0),
        new Container(padding: EdgeInsets.only(right:180.0), child :Text("Phone Number or Email")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            hintText: 'Email',
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          ),
          validator: FormValidator().validateEmail,
          onSaved: (String value) {
            _email = value;
          },
        ),
        new SizedBox(height: 50.0),
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("Password")),
        new TextFormField(
            cursorColor: Colors.orange,
            autofocus: false,
            obscureText: _obscureText,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: 'Password',
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.orange),
              ),
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;});
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  semanticLabel:
                  _obscureText ? 'show password' : 'hide password',
                ),
              ),
            ),
            validator: FormValidator().validatePassword,
            onSaved: (String value) {
              _password = value;
            }),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: (){login();},
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text('Log In', style: TextStyle(color: Colors.white)),
          ),
        ),
        new FlatButton(
          child: Text('Forgot password?',
            style: TextStyle(color: Colors.black54),
          ),
          //onPressed: _sendToResetPage,
        ),
        new FlatButton(
          onPressed: _sendToRegisterPage,
          child: Text('Create a new Account',
              style: TextStyle(color: Colors.black54)),
        ),
      ],
    );
  }

  _sendToRegisterPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrationPage()),
    );
  }

  _sendToHomepage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()),
    );
  }

 bool _validateInputs(){
    final FormState form = _key.currentState;
    if(form.validate()){
      form.save();
      print("email : $_email");
      print("password : $_password");
      return true;
    }
    else{
      print("not valid..");
      return false;
    }
  }

  void login() async {
//   if(_validateInputs()){
     try{
       FirebaseUser user = await FirebaseAuth.instance.signInWithEmailAndPassword(email: _email, password: _password);
       print("Sign in : ${user.uid}");
     }
     catch(e){
       print("No user Login : $e");
     }
     return _sendToHomepage();
//   }
  }
}
