import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/Preferences.dart';
import 'package:shopping_cart/model/validation.dart';
import 'package:shopping_cart/screens/ShopLogin.dart';
import 'package:shopping_cart/screens/UserSignupPage.dart';
import 'package:shopping_cart/screens/UserloginPage.dart';
import 'package:shopping_cart/Preferences.dart';
class RegistrationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.white,
      ),
      home: ShopRegistrationPage(),
    );
  }
}

class ShopRegistrationPage extends StatefulWidget {
  ShopRegistrationPage({ Key key }) : super(key: key);
  @override
  _ShopRegistrationPageState createState() => new _ShopRegistrationPageState();
}

class _ShopRegistrationPageState extends State<ShopRegistrationPage> {
  final DBRef = FirebaseDatabase.instance.reference();
  //static final newDBref = FirebaseDatabase.instance.reference();
  final db = Firestore.instance;
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  //LoginRequestData _loginData = LoginRequestData();
  bool _obscureText = true;
  final _passwordController = TextEditingController();

  String address;
  String shop_name;
  String _email;
  String _password;
  String confirm_password;
  String contact_number;
  String id;

  String errorMsg;
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          title: Center(
            child: (Text("Shop Registration Page", style: TextStyle(color: Colors.white))),
          )),
      body: new Center(
        child: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("Shop Name")),
        new SizedBox(height: 5.0),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            //   hintText: MyStrings.firstname_hint,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
          validator: FormValidator().validateFirstName,
          onSaved: (String value) {
            shop_name = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("address")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            //   hintText: MyStrings.lastname_hint,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
          validator: FormValidator().validateLastName,
          onSaved: (String value) {
            address= value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:290.0), child :Text("Email")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            //      hintText: MyStrings.email_hint,
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
//          validator: FormValidator().validateEmail,
          onSaved: (String value) {
            _email = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:290.0), child :Text("shop_id")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            //      hintText: MyStrings.email_hint,
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
          onSaved: (String value) {
            id = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("Password")),
        new TextFormField(
            key: passKey,
            cursorColor: Colors.orange,
            autofocus: false,
            obscureText: _obscureText,
            keyboardType: TextInputType.text,
            controller: _passwordController,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.orange),
              ),
              //    hintText: MyStrings.password_hint,

              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  semanticLabel:
                  _obscureText ? 'show password' : 'hide password',
                ),
              ),
            ),
            validator: FormValidator().validatePassword,
            onSaved: (String value) {
              _password = value;
            }),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:210.0), child :Text("Confirm Password")),
        new TextFormField(
            cursorColor: Colors.orange,
            autofocus: false,
            obscureText: _obscureText,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.orange),
              ),
              // hintText: MyStrings.confirmpassword_hint,
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;});
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  semanticLabel:
                  _obscureText ? 'show password' : 'hide password',
                ),
              ),
            ),
            validator: FormValidator().validatePasswordMatching,
            onSaved: (String value) {
              confirm_password = value;
            }
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:210.0), child :Text("Contact Number")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            //hintText: 'Contact Number',
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          ),
          // validator: FormValidator().validateEmail,
          onSaved: (String value) {
            contact_number = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: (){
              Shopregister();
              //writedata();
              },
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text('Register', style: TextStyle(color: Colors.white)),
          ),
        ),

        new FlatButton(
          onPressed: (){
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => LoginPage()));
          },
          child: Text('Back to Login',
              style: TextStyle(color: Colors.black54)),
        ),
      ],
    );
  }

  bool _validateInputs(){
    final FormState form = _key.currentState;
    if(form.validate()){
      form.save();
      print("email : $shop_name");
      Preferences.setAppToken(shop_name);
      Preferences.setLoggedIn(true);
      print("password : $address");
      print("email : $_email");
      print("password : $_password");
      print("email : $confirm_password");
      print("email : $contact_number");
      return true;
    }
    else{
      print("not valid..");
      return false;
    }
  }

  void writedata(){
       DBRef.child("Shop_Registration").child(shop_name).set({
      'confirm_password' : confirm_password,
      "password" : _password,
      "shop_address": address,
      "shop_contact_number": contact_number,
      "shop_emal_id": _email,
      "shop_id" : id,
         "shop_name" : shop_name
    });
  }
  sendShopLogin(){
    return Navigator.push(context, MaterialPageRoute(builder: (context) => ShopLoginScreen()));
  }

String newMessege;
  void Shopregister() async{
//    if(_validateInputs()){
      try{
        FirebaseUser newUser = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _password);
        print("Register User : ${newUser.uid}");
        writedata();
        print(shop_name);
        return sendShopLogin();
      }catch(e){
        print("Error data : $e");
      }
    }
//  }
}

