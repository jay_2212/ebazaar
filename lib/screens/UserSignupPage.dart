import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/model/validation.dart';
import 'package:shopping_cart/screens/UserloginPage.dart';
class RegistrationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.white,
      ),
      home: RegistrationPage(),
    );
  }
}

class RegistrationPage extends StatefulWidget {
  RegistrationPage({ Key key }) : super(key: key);
  @override
  _RegistrationPageState createState() => new _RegistrationPageState();
}

var passKey = GlobalKey<FormFieldState>();
class _RegistrationPageState extends State<RegistrationPage> {
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  //LoginRequestData _loginData = LoginRequestData();
  bool _obscureText = true;
  final _passwordController = TextEditingController();
  String first_name;
  String last_name;
  String _email;
  String _password;
  String confirm_password;
  String address;
  String contact_number;

  String errorMsg;
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          title: Center(
            child: (Text("Registration Page", style: TextStyle(color: Colors.white))),
          )),
      body: new Center(
        child: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("First Name")),
        new SizedBox(height: 5.0),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            //   hintText: MyStrings.firstname_hint,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
          validator: FormValidator().validateFirstName,
          onSaved: (String value) {
            first_name = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("Last Name")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            //   hintText: MyStrings.lastname_hint,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
          validator: FormValidator().validateLastName,
          onSaved: (String value) {
            last_name= value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:290.0), child :Text("Email")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            //      hintText: MyStrings.email_hint,
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 15.0),
          ),
          validator: FormValidator().validateEmail,
          onSaved: (String value) {
            _email = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:260.0), child :Text("Password")),
        new TextFormField(
            key: passKey,
            cursorColor: Colors.orange,
            autofocus: false,
            obscureText: _obscureText,
            keyboardType: TextInputType.text,
            controller: _passwordController,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.orange),
              ),
              //    hintText: MyStrings.password_hint,

              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  semanticLabel:
                  _obscureText ? 'show password' : 'hide password',
                ),
              ),
            ),
            validator: FormValidator().validatePassword,
            onSaved: (String value) {
              _password = value;
            }),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:210.0), child :Text("Confirm Password")),
        new TextFormField(
            cursorColor: Colors.orange,
            autofocus: false,
            obscureText: _obscureText,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.orange),
              ),
              // hintText: MyStrings.confirmpassword_hint,
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;});
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  semanticLabel:
                  _obscureText ? 'show password' : 'hide password',
                ),
              ),
            ),
            validator: FormValidator().validatePasswordMatching,
            onSaved: (String value) {
              confirm_password = value;
            }
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:250.0), child :Text("Address")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
           // hintText: 'Address',
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          ),
         // validator: FormValidator().validateEmail,
          onSaved: (String value) {
            address = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Container(padding: EdgeInsets.only(right:210.0), child :Text("Contact Number")),
        new TextFormField(
          cursorColor: Colors.orange,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            //hintText: 'Contact Number',
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange),
            ),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          ),
         // validator: FormValidator().validateEmail,
          onSaved: (String value) {
            contact_number = value;
          },
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: (){register();},
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text('Register', style: TextStyle(color: Colors.white)),
          ),
        ),

        new FlatButton(
          onPressed: (){
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => LoginPage()));
          },
          child: Text('Back to Login',
              style: TextStyle(color: Colors.black54)),
        ),
      ],
    );
  }

  bool _validateInputs(){
    final FormState form = _key.currentState;
    if(form.validate()){
      form.save();
      print("email : $first_name");
      print("password : $last_name");
      print("email : $_email");
      print("password : $_password");
      print("email : $confirm_password");
      print("password : $address");
      print("email : $contact_number");
      return true;
    }
    else{
      print("not valid..");
      return false;
    }
  }

  void register() async{
    if(_validateInputs()){
      try{
        FirebaseUser newUser = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _password);
        print("Register User : ${newUser.uid}");
      }catch(e){
        print("Error : $e");
      }
    }
  }
}

