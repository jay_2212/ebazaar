import 'dart:core';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/ui/AvailableList.dart';

class AvailableShopItems extends StatefulWidget {
  @override
  _AvailableShopItemsState createState() => _AvailableShopItemsState();
}
String data_inShop;
class _AvailableShopItemsState extends State<AvailableShopItems> {


  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  final DBRef = FirebaseDatabase.instance.reference();
  List temp = new List();
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Center(
            child: (Text("shop data Page", style: TextStyle(color: Colors.white))),
          )),
      body: new Center(
        child: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("enter your shop name ")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'please enter your shop name to see your data',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  data_inShop = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                 // onlyShop();
                },
              ),
            ),
          ],
        ),

        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              //RecipeListPage().createState().seeadditems();
              seeadditems();
              Navigator.push(context, MaterialPageRoute(builder: (context) => AvailableList()));
            },
            color: Colors.orange,
            child: Text(
              'See Add Items',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),

      ],
    );
  }
  String dataList;
  List seeadditems() {
    print("data");
    DBRef.child("Shop_List").child(data_inShop).once().then((DataSnapshot snapshot) {
      var item = snapshot.key;
      print(item);
      var data = snapshot.value;
      print(data);
      for (dataList in data.keys) {
        temp.add(dataList);
      }
    });
    return temp;
  }

}

