import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/ui/AddedItemList.dart';
import 'package:shopping_cart/ui/ShopDetails.dart';

import 'ItemCategories.dart';
import 'ItemDetailsPage.dart';

//class ItemList extends StatefulWidget {
//  ItemList({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  _ItemList createState() => _ItemList();
//}
//String ItemType;
//class _ItemList extends State<ItemList> {
// List newItemList = new List();
//  List itemDetails = new List();
//  String itemData;
//  String shop_name;
//  String Item_categories;
//  String item_name;
//  final DBRef = FirebaseDatabase.instance.reference();
//  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//  bool isLoading;
//  ScrollController _controller;
//
//  _scrollListener() {
//    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
//    if (_controller.offset >= _controller.position.maxScrollExtent &&
//        !_controller.position.outOfRange) {
//      setState(() {});
//    }
//    if (_controller.offset <= _controller.position.minScrollExtent &&
//        !_controller.position.outOfRange) {
//      setState(() {});
//    }
//  }
//
////  List<Items> items = new List();
//
//  @override
//  void initState() {
//    _controller = ScrollController();
//    _controller.addListener(_scrollListener);
//    seeData();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Center(child: Text("ItemType")),
//          actions: <Widget>[
//            IconButton(
//                icon: Icon(Icons.add),
//                onPressed: () {
//                  setState(() {
//                    seeData();
//                  });
//                  initState();
//                })
//          ],
//        ),
//        body: SingleChildScrollView(
//            child: Container(
//                child: Column(
//                  children: <Widget>[
//                    if (newItemList != null)
//                      for (int i = 0; i < newItemList.length; i++)
//                        ListTile(
//                          title: Text(newItemList == null
//                              ? "Please tap on + butto to refresh the UI"
//                              : newItemList[i]),
//                          trailing: Icon(Icons.keyboard_arrow_right,
//                              color: Colors.black, size: 30.0),
//                          onTap: () {
//                            print(newItemList[i]);
//                            itemDetails.add(newItemList[i]);
//                            ItemType = itemDetails.toString();
//                            itemDetails.clear();
//                            print("tempData : $itemDetails");
//                            Navigator.push(context, MaterialPageRoute(builder: (context) => DetailOfItemPage()));
//                          },
//                        )
//                  ],
//                ))),
//        floatingActionButton: new FloatingActionButton(
//            elevation: 0.0,
//            child: new Icon(Icons.check),
//            backgroundColor: new Color(0xFFE57373),
//            onPressed: (){
//              setState(() {
//                seeData();
//              });
//            }
//        ));
//  }
//
//  String shopDataList;
//  List seeData() {
//    print("data");
//    DBRef.child("Shop_List")
//        .child(ShopData.substring(1, ShopData.length - 1)).child(ItemCategory.substring(1, ItemCategory.length - 1))
//        .once()
//        .then((DataSnapshot snapshot) {
//      var item = snapshot.key;
//      print(item);
//      var data = snapshot.value;
//      print("Data: $data");
//      for (shopDataList in data.keys) {
//        newItemList.add(shopDataList);
//        print(newItemList);
//      }
//    });
//    return newItemList;
//  }
//
//  @override
//  void dispose() {
//    _controller.dispose();
//    super.dispose();
//  }
//
//  String dataList;
//}
//
//class Items {
//  String shop_name;
//  String Item_categories;
//  String item_name;
//  String item_price;
//  String item_quantity;
//  String item_count;
//
//  Items(
//      this.shop_name,
//      this.Item_categories,
//      this.item_name,
//      this.item_price,
//      this.item_quantity,
//      this.item_count,
//      );
//
//  factory Items.fromJson(dynamic json) {
//    return Items(
//        json['shop_name'] as String,
//        json['Item_categories'] as String,
//        json['item_name'] as String,
//        json['item_price'] as String,
//        json['item_quantity'] as String,
//        json['item_count'] as String);
//  }
//
//  @override
//  String toString() {
//    return '{ ${this.shop_name}, ${this.Item_categories},${this.item_name},${this.item_price},${this.item_quantity},${this.item_count} }';
//  }
//
//  Map<String, dynamic> toJson() => {
//    "shop_name": shop_name,
//    "Item_categories": Item_categories,
//    "item_name": item_name,
//    "item_price": item_price,
//    "item_quantity": item_quantity,
//    "item_count": item_count,
//  };
//}