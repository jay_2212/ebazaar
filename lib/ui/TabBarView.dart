import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/ui/AddtoCart.dart';
import 'AddedItemList.dart';
import 'ItemCategories.dart';
import 'ItemDetailsPage.dart';
import 'ItemType.dart';


TabController controller;
int count = 0;
class TabbarPage extends StatefulWidget {
  TabbarPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabbarPage createState() => _TabbarPage();
}
void stateChange(){
  RaisedButton(
    onPressed: (){
      count + 1;
    },
  );
}
String ShopData;
class _TabbarPage extends State<TabbarPage> with SingleTickerProviderStateMixin {
  TabController controller;
  final DBRef = FirebaseDatabase.instance.reference();
  ScrollController _controller;

  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    controller = new TabController(vsync: this, length: 4, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(22.0))
          ),child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex : 1,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: TextFormField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Search Here",
                      hintStyle: TextStyle(color: Colors.black),
                    ),
                  )
              ),
            ),
            Expanded(
              flex: 0,
              child:Container(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child:  Row(
                  children: <Widget>[
                    IconButton(
                      onPressed: (){
                      },
                      icon: Icon(Icons.search, color: Colors.black),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        ),
        bottom: TabBar(
            controller: controller,
            indicatorColor: Colors.white,
            labelColor: Colors.orange,
            unselectedLabelColor: Colors.white,
            tabs: <Widget>
            [
              Tab(text: "shops"),
              Tab(text: "item_categories"),
              Tab(text: "item_names"),
              Tab(text: "item_details",),
            ]
        ),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.view_list), onPressed: () {
            setState(() {
            });
            initState();
          })
        ],
      ),
      body: TabBarView(
        controller: controller,
        children: <Widget>[
          RecipeListPage(),
          ItemCategoriesList(),
          ItemList(),
          DetailOfItemPage(),
        ],
      ),
      );

  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String dataList;
}

class Items{
  String shop_name;
  String Item_categories;
  String item_name;
  String item_price;
  String item_quantity;
  String item_count;

  Items(
      this.shop_name,
      this.Item_categories,
      this.item_name,
      this.item_price,
      this.item_quantity,
      this.item_count,
      );


  factory Items.fromJson(dynamic json) {
    return Items(
        json['shop_name'] as String,
        json['Item_categories'] as String,
        json['item_name'] as String,
        json['item_price'] as String,
        json['item_quantity'] as String,
        json['item_count'] as String);
  }

  @override
  String toString() {
    return '{ ${this.shop_name}, ${this.Item_categories},${this.item_name},${this.item_price},${this.item_quantity},${this.item_count} }';
  }

  Map<String, dynamic> toJson() => {
    "shop_name" : shop_name,
    "Item_categories" : Item_categories,
    "item_name" : item_name,
    "item_price": item_price,
    "item_quantity": item_quantity,
    "item_count": item_count,
  };
}

class RecipeListPage extends StatefulWidget {
  RecipeListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RecipeListPage createState() => _RecipeListPage();
}
class _RecipeListPage extends State<RecipeListPage> with SingleTickerProviderStateMixin {

  String Shopnames;

  List temp = new List();
  List tempData = new List();

  String shop_name;
  String Item_categories;
  String item_name;
  final DBRef = FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading;
  ScrollController _controller;

  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

  List items = new List();

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    seeadditems();
    controller = new TabController(vsync: this, length: 4, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:  SingleChildScrollView(
          child: Container(
              child : Column(
                children: <Widget>[
                  if(temp !=null)
                    for(int i = 0; i< temp.length; i++)
                      ListTile(
                        title: Text(temp==null ? "Please tap on + butto to refresh the UI" : temp[i]),
                        trailing: Icon(Icons.keyboard_arrow_right,
                            color: Colors.black, size: 30.0),
                        onTap: (){
                          // print item categories from shop from shop
                          print(temp[i]);
                          // store Item category in the new List.
                          tempData.add(temp[i]);
                          // convert list in too String
                          ShopData = tempData.toString();
                          tempData.clear();
                          stateChange();
                        },
                      )
                ],
              )
          ),
        ),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Text("show"),
            backgroundColor: new Color(0xFFE57373),
            onPressed: (){
              setState(() {
                seeadditems();
              });
            }
        )
    );

  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String dataList;

  List seeadditems() {
    print("data");
    DBRef.child("Shop_List").once().then((DataSnapshot snapshot) {
      var item = snapshot.key;
      print(item);
      var data = snapshot.value;
      print(data);
      for (dataList in data.keys) {
        temp.add(dataList);
        //print(temp);
      }
      //  print("jay hdud : $Shopnames");
      //temp.clear();
    });
    return temp;
  }

}






class ItemCategoriesList extends StatefulWidget {
  ItemCategoriesList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ItemCategoriesList createState() => _ItemCategoriesList();
}
String ItemCategory;
class _ItemCategoriesList extends State<ItemCategoriesList> {
  List newData = new List();
  String Shopnames;
  List temp = new List();
  List shopItems = new List();
  String itemData;
  String shop_name;
  String Item_categories;
  String item_name;
  final DBRef = FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading;
  ScrollController _controller;

  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

//  List<Items> items = new List();

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    seeData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(/*
        appBar: AppBar(
          title: Center(child: Text("ShopList")),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    seeData();
                  });
                  initState();
                })
          ],
        ),*/
        body: SingleChildScrollView(
            child: Container(
                child: Column(
                  children: <Widget>[
                    if (newData != null)
                      for (int i = 0; i < newData.length; i++)
                        ListTile(
                          title: Text(newData == null
                              ? "Please tap on + butto to refresh the UI"
                              : newData[i]),
                          trailing: Icon(Icons.keyboard_arrow_right,
                              color: Colors.black, size: 30.0),
                          onTap: () {
                            // print Items from item_categories.
                            print(newData[i]);
                            // add Item in to List
                            shopItems.add(newData[i]);
                            // Convert List to String.
                            ItemCategory = shopItems.toString();
                            shopItems.clear();
                            print("shopItems : $shopItems");
                         //   Navigator.push(context, MaterialPageRoute(builder: (context) => ItemList()));
                          },
                        )
                  ],
                )
            )),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.check),
            backgroundColor: new Color(0xFFE57373),
            onPressed: (){
              setState(() {
                seeData();
              });
            }
        ));
  }

  String shopDataList;
  List seeData() {
    print("data");
    DBRef.child("Shop_List")
        .child(ShopData.substring(1, ShopData.length - 1))
        .once()
        .then((DataSnapshot snapshot) {
      var item = snapshot.key;
      print(item);
      var data = snapshot.value;
      print(data);
      for (shopDataList in data.keys) {
        newData.add(shopDataList);
        print(newData);
      }
    });
    return newData;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String dataList;
}







class ItemList extends StatefulWidget {
  ItemList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ItemList createState() => _ItemList();
}
String ItemType;
class _ItemList extends State<ItemList> {
  List newItemList = new List();
  List itemDetails = new List();
  String itemData;
  String shop_name;
  String Item_categories;
  String item_name;
  final DBRef = FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading;
  ScrollController _controller;

  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

//  List<Items> items = new List();

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    seeData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /*appBar: AppBar(
          title: Center(child: Text("ItemType")),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    seeData();
                  });
                  initState();
                })
          ],
        ),*/
        body: SingleChildScrollView(
            child: Container(
                child: Column(
                  children: <Widget>[
                    if (newItemList != null)
                      for (int i = 0; i < newItemList.length; i++)
                        ListTile(
                          title: Text(newItemList == null
                              ? "Please tap on + butto to refresh the UI"
                              : newItemList[i]),
                          trailing: Icon(Icons.keyboard_arrow_right,
                              color: Colors.black, size: 30.0),
                          onTap: () {
                            print(newItemList[i]);
                            itemDetails.add(newItemList[i]);
                            ItemType = itemDetails.toString();
                            itemDetails.clear();
                            print("tempData : $itemDetails");
                           // Navigator.push(context, MaterialPageRoute(builder: (context) => DetailOfItemPage()));
                          },
                        )
                  ],
                ))),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.check),
            backgroundColor: new Color(0xFFE57373),
            onPressed: (){
              setState(() {
                seeData();
              });
            }
        ));
  }

  String shopDataList;
  List seeData() {
    print("data");
    DBRef.child("Shop_List")
        .child(ShopData.substring(1, ShopData.length - 1)).child(ItemCategory.substring(1, ItemCategory.length - 1))
        .once()
        .then((DataSnapshot snapshot) {
      var item = snapshot.key;
      print(item);
      var data = snapshot.value;
      print("Data: $data");
      for (shopDataList in data.keys) {
        newItemList.add(shopDataList);
        print(newItemList);
      }
    });
    return newItemList;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String dataList;
}

class DetailOfItemPage extends StatefulWidget {
  DetailOfItemPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DetailOfItemPage createState() => _DetailOfItemPage();
}
class _DetailOfItemPage extends State<DetailOfItemPage> {
  final DBRef = FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading;
  List<Items> items = new List();
  ScrollController _controller;
  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    dataitems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       /* appBar: AppBar(
          title: Center(child : Text("Items")),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    dataitems();
                  });
                  initState();
                })
          ],
        ),*/
        body: Container(
            child:
            new Column(
              children: <Widget>[
                Expanded(
                    child: FutureBuilder(
                      builder: (BuildContext context, snapshot) {
                        //List<Recipe> recepies = snapshot.data;
                        print("data here : $items");
                        return ListView.separated(
                          controller: _controller,
                          itemCount: items.length,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                                title: Container(
                                    child: new Text(items[index].item_name, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 30),)
                                ),
                                subtitle: Container(
                                    child :Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(items[index].item_count),
                                        new Text(items[index].item_quantity),
                                        new Text(items[index].item_price),
                                      ],
                                    )
                                ),
                                trailing: Icon(Icons.add_shopping_cart,
                                    color: Colors.black, size: 30.0),
                                onTap: () {
                                  DBRef.child("Add_to_Cart").child(items[index].item_name).set({
                                    "shop_name" : items[index].shop_name,
                                    "Item_categories" : items[index].Item_categories,
                                    "item_name" : items[index].item_name,
                                    "item_price" : items[index].item_price,
                                  });
                                 // Navigator.push(context, MaterialPageRoute(builder: (context) => AddtoCart()));
                                });
                          },
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                        );
                      },
                    ))
              ],
            )
        ),floatingActionButton: new FloatingActionButton(
        elevation: 0.0,
        child: new Icon(Icons.check),
        backgroundColor: new Color(0xFFE57373),
        onPressed: (){
          setState(() {
            Navigator.push(context, MaterialPageRoute(builder: (context) => AddtoCart()));
          });
        }
    ));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  List dataitems(){
    DBRef.child("Shop_List")
        .child(ShopData.substring(1, ShopData.length - 1)).
    child(ItemCategory.substring(1, ItemCategory.length - 1)).child(ItemType.substring(1, ItemType.length - 1))
        .once()
        .then((DataSnapshot snapshot) {
      print('Data : ${snapshot.value}');
      var data = snapshot.value;
      Items shopItems = Items.fromJson(data);
      items.add(shopItems);
      print("items : $items");
    });
    return items;
  }

}


