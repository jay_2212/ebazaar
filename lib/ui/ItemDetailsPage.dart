//
//import 'package:firebase_database/firebase_database.dart';
//import 'package:flutter/material.dart';
//import 'package:shopping_cart/ui/ItemType.dart';
//
//import 'AddedItemList.dart';
//import 'ItemCategories.dart';
//
//
//class DetailOfItemPage extends StatefulWidget {
//  DetailOfItemPage({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  _DetailOfItemPage createState() => _DetailOfItemPage();
//}
//class _DetailOfItemPage extends State<DetailOfItemPage> {
//  final DBRef = FirebaseDatabase.instance.reference();
//  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//  bool isLoading;
//  List<Items> items = new List();
//  ScrollController _controller;
//  _scrollListener() {
//    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
//    if (_controller.offset >= _controller.position.maxScrollExtent &&
//        !_controller.position.outOfRange) {
//      setState(() {});
//    }
//    if (_controller.offset <= _controller.position.minScrollExtent &&
//        !_controller.position.outOfRange) {
//      setState(() {});
//    }
//  }
//
//  @override
//  void initState() {
//    _controller = ScrollController();
//    _controller.addListener(_scrollListener);
//    dataitems();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Center(child : Text("Items")),
//          actions: <Widget>[
//            IconButton(
//                icon: Icon(Icons.add),
//                onPressed: () {
//                  setState(() {
//                    dataitems();
//                  });
//                  initState();
//                })
//          ],
//        ),
//        body: Container(
//            child:
//            new Column(
//              children: <Widget>[
//                Expanded(
//                    child: FutureBuilder(
//                      builder: (BuildContext context, snapshot) {
//                        //List<Recipe> recepies = snapshot.data;
//                        print("data here : $items");
//                        return ListView.separated(
//                          controller: _controller,
//                          itemCount: items.length,
//                          shrinkWrap: true,
//                          itemBuilder: (BuildContext context, int index) {
//                            return ListTile(
//                                title: Container(
//                                    child: new Text(items[index].item_name, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 30),)
//                                ),
//                                subtitle: Container(
//                                    child :Column(
//                                      mainAxisAlignment: MainAxisAlignment.start,
//                                      crossAxisAlignment: CrossAxisAlignment.start,
//                                      children: <Widget>[
//                                        new Text(items[index].item_count),
//                                        new Text(items[index].item_quantity),
//                                        new Text(items[index].item_price),
//                                      ],
//                                    )
//                                ),
//                                trailing: Icon(Icons.keyboard_arrow_right,
//                                    color: Colors.black, size: 30.0),
//                                onTap: () {
//
//                                });
//                          },
//                          separatorBuilder: (context, index) {
//                            return Divider();
//                          },
//                        );
//                      },
//                    ))
//              ],
//            )
//        ),floatingActionButton: new FloatingActionButton(
//        elevation: 0.0,
//        child: new Icon(Icons.check),
//        backgroundColor: new Color(0xFFE57373),
//        onPressed: (){
//          setState(() {
//            dataitems();
//          });
//        }
//    ));
//  }
//
//  @override
//  void dispose() {
//    _controller.dispose();
//    super.dispose();
//  }
//
//  List dataitems(){
//    DBRef.child("Shop_List")
//        .child(ShopData.substring(1, ShopData.length - 1)).
//    child(ItemCategory.substring(1, ItemCategory.length - 1)).child(ItemType.substring(1, ItemType.length - 1))
//        .once()
//        .then((DataSnapshot snapshot) {
//      print('Data : ${snapshot.value}');
//      var data = snapshot.value;
//      Items shopItems = Items.fromJson(data);
//      items.add(shopItems);
//      print("items : $items");
//    });
//    return items;
//  }
//
//}
//
//
//class Items {
//  String item_name;
//  String item_price;
//  String item_quantity;
//  String item_count;
//
//  Items(this.item_name,
//      this.item_price,
//      this.item_quantity,
//      this.item_count,);
//
//  factory Items.fromJson(dynamic json) {
//    return Items(json['item_name'] as String,
//        json['item_price'] as String,
//        json['item_quantity'] as String,
//        json['item_count'] as String);
//  }
//
//  @override
//  String toString() {
//    return '{ ${this.item_name}, ${this.item_price}, ${this
//        .item_quantity}, ${this.item_count} }';
//  }
//
//
//  Map<String, dynamic> toJson() =>
//      {
//        "item_name": item_name,
//        "item_price": item_price,
//        "item_quantity": item_quantity,
//        "item_count": item_count,
//      };
//}