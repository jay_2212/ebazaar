
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/ui/AddedItemList.dart';
import 'package:shopping_cart/ui/ShopDetails.dart';


class ShopListPage extends StatefulWidget {
  ShopListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ShopListPage createState() => _ShopListPage();
}
class _ShopListPage extends State<ShopListPage> {
  String shop_name;
  String Item_categories;
  String item_name;
  final DBRef = FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading;
  ScrollController _controller;
  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {
    }
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }
  List<Items> items = new List();
  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child : Text("Items")),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.add), onPressed: (){
//            print("shop_name : $Shop");
              seeadditems();
            })
          ],
        ),
        body:
            Container(
                child: new Form(
                  key: _formKey,
                  child: RecipeListScreenUI(),
                )));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget RecipeListScreenUI() {
    return new Column(
      children: <Widget>[
        Expanded(
            child: FutureBuilder(
              //future: RecipeList,
              builder: (BuildContext context, snapshot) {
                //List<Recipe> recepies = snapshot.data;
                return ListView.separated(
                  controller: _controller,
                  itemCount: items.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        title: Container(
                            child: new Text(items[index].shop_name, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 30),)
                        ),
                        trailing: Icon(Icons.keyboard_arrow_right,
                            color: Colors.black, size: 30.0),
                        onTap: () {

                        });
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              },
            ))
      ],
    );
  }

  void seeadditems(){
    DBRef.child("Only_Shop_List").once().then((DataSnapshot snapshot) {
      print('Data : ${snapshot.value}');
      var data = snapshot.value;
      setState(() {
        var details = data;
        print(details);
        Items shopItems = Items.fromJson(details);
        items.add(shopItems);
        print(items);
      });

    });
  }
}

class Items{
  String shop_name;

  Items(
      this.shop_name,
      );


  factory Items.fromJson(dynamic json) {
    return Items(
        json['shop_name'] as String);
  }

  @override
  String toString() {
    return '{ ${this.shop_name}}';
  }

  Map<String, dynamic> toJson() => {
    "shop_name" : shop_name,
  };
}