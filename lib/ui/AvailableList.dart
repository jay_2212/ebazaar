import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/screens/ShopLogin.dart';
import 'package:shopping_cart/screens/UserloginPage.dart';
import 'package:shopping_cart/ui/first_screen.dart';
import 'package:shopping_cart/ui/proceedTobuy.dart';
import 'package:shopping_cart/ui/shopKepeerData.dart';

import 'TabBarView.dart';

class AvailableList extends StatefulWidget {
  AvailableList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AvailableList createState() => _AvailableList();
}

class _AvailableList extends State<AvailableList> {
  List itemDetails = new List();
  List temp = new List();
  List temp1 = new List();
  String dataList;
  String AvailableList;
  String subListFromAvailableList;
  final DBRef = FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading;
  List items = new List();
  List itemsData = new List();
  ScrollController _controller;
  _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {}
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    dataitems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text("data Availablevin Shops")),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  return shopSignout();
                //  initState();
                }),
            IconButton(
                icon: Icon(Icons.show_chart),
                onPressed: () {
                  setState(() {
                    dataitems();
                  });
                }),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              if (items != null)
                for (int i = 0; i < items.length; i++)
                  ExpansionTile(
                    title: new Text(items[i]),
                    children: <Widget>[
                      FutureBuilder(
                        builder: (BuildContext context, snapshot) {
                          print("here : ${items[i].toString()}");
//                          itemDetails.add(items[i]);
                          AvailableList = items[i].toString();

                          //print("here data of 1 key  : $AvailableList");

                          DBRef.child("Shop_List").child(data_inShop).child(AvailableList).once()
                              .then((DataSnapshot snapshot) {
                            var item = snapshot.key;
                            print("item : $item");
                            var data = snapshot.value;
                            print(data);
                            for (dataList in data.keys) {
                              temp1.add(dataList);
                               subListFromAvailableList = temp1[i].toString();
                              print("data in String : $subListFromAvailableList");
                            }
                          });

                          DBRef.child("Shop_List").child(data_inShop).child(AvailableList).child(subListFromAvailableList)
                              .once()
                              .then((DataSnapshot snapshot) {
                            print('Data : ${snapshot.value}');
                            var data = snapshot.value;
                            Items shopItems = Items.fromJson(data);
                            temp.add(shopItems);
                            print("items : $temp");
                            if(temp.length != 1){
                              temp.clear();
                            }
                          });

                          return ListView.builder(
                            controller: _controller,
                            itemCount: temp.length,
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Container(
                                  child: new Text("Shop Name : ${temp[index].shop_name}"),
                                ),
                                subtitle: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[

                                      new Text("Item Price : ${temp[index].item_price}"),
                                      new Text("Item_categories : ${temp[index].Item_categories}"),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      )
                    ],
                  ),
            ],
          ),
        ),
        floatingActionButton: new RaisedButton(
            elevation: 0.0,
            child: new Text(
              "Proceed to pay",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProceedToBuy()));
            }));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }


  List dataitems() {
    print("data");
    DBRef.child("Shop_List")
        .child(data_inShop)
        .once()
        .then((DataSnapshot snapshot) {
      var item = snapshot.key;
      print("item : $item");
      var data = snapshot.value;
      print(data);
      for (dataList in data.keys) {
        items.add(dataList);
      }
      print("data shop : $items");
    });
    return items;
  }

  void shopSignout() async {
    try{
      await FirebaseAuth.instance.signOut();
       Navigator.push(context, MaterialPageRoute(builder: (context) => FirstScreen()));
    }
    catch(e){
      print("No user Login : $e");
    }
  }

  List subdata() {
    return temp;
  }
}

class Items {
  String shop_name;
  String Item_categories;
  String item_name;
  String item_price;

  Items(
    this.shop_name,
    this.Item_categories,
    this.item_name,
    this.item_price,
  );

  factory Items.fromJson(dynamic json) {
    return Items(json['shop_name'] as String, json['Item_categories'] as String,
        json['item_name'] as String, json['item_price'] as String);
  }

  @override
  String toString() {
    return '{ ${this.shop_name}, ${this.Item_categories},${this.item_name},${this.item_price} }';
  }

  Map<String, dynamic> toJson() => {
        "shop_name": shop_name,
        "Item_categories": Item_categories,
        "item_name": item_name,
        "item_price": item_price,
      };
}
