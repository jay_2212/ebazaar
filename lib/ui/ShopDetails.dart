import 'dart:core';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/model/validation.dart';
import 'package:shopping_cart/screens/ShopRegistration.dart';
import 'package:shopping_cart/screens/UserloginPage.dart';
import 'package:shopping_cart/ui/AddedItemList.dart';
import 'package:shopping_cart/ui/AddtoCart.dart';
import 'package:shopping_cart/ui/shopKepeerData.dart';

import 'TabBarView.dart';
import 'first_screen.dart';

class ShopFirstScreen extends StatefulWidget {
  @override
  _ShopFirstScreenState createState() => _ShopFirstScreenState();
}

class _ShopFirstScreenState extends State<ShopFirstScreen> {
   String shop_name;
   String item_name;
   String item_price;
   String item_quantity;
   String item_count;
   String Item_categories;

   GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  final DBRef = FirebaseDatabase.instance.reference();
List temp = new List();
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
          title: Center(
        child: (Text("Shopkeeper Page", style: TextStyle(color: Colors.white))),
      )),
      body: new Center(
        child: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[new Text("Shop_name")],
        )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'Shop name',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  shop_name = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  onlyShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[new Text("Item Categories")],
        )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'item categories',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  Item_categories = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("Item Name")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'item name',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  item_name = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("Item price")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'item_price',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  item_price = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("Item quantity")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'item quantity',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  item_quantity = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("Item count")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'item count ',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  item_count = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              //Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
              addShop();
            },
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text(
              'Add Items',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {

              Navigator.push(context, MaterialPageRoute(builder: (context) => AddtoCart()));
            },
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text(
              'Add to cart',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              RecipeListPage().createState().seeadditems();
              seeadditems();
              Navigator.push(context, MaterialPageRoute(builder: (context) => AvailableShopItems()));
             // Navigator.push(context, MaterialPageRoute(builder: (context) => TabbarPage()));
            },
            color: Colors.orange,
            child: Text(
              'See Add Items',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),

      ],
    );
  }
  String dataList;
   List seeadditems() {
     print("data");
     DBRef.child("Shop_List").once().then((DataSnapshot snapshot) {
       var item = snapshot.key;
       print(item);
       var data = snapshot.value;
       print(data);
       for (dataList in data.keys) {
         temp.add(dataList);
         //print(temp);
       }
       //  print("jay hdud : $Shopnames");
       //temp.clear();
     });
     return temp;
   }

   void addShop(){
    DBRef.child("Shop_List").child(shop_name).child(Item_categories).child(item_name).set({
      "shop_name" : shop_name,
      "Item_categories" : Item_categories,
      "item_name" : item_name,
      "item_price" : item_price,
      "item_quantity": item_quantity,
      "item_count": item_count,
    });
  }
  void onlyShop(){

  }
}

