import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shopping_cart/screens/ShopLogin.dart';
import 'package:shopping_cart/screens/UserloginPage.dart';
import 'package:shopping_cart/ui/cart_product_details.dart';
import 'package:shopping_cart/ui/similar_products.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: new Text("Ebazaar")
        ),
      ),
        body: Column(
      children: <Widget>[
        Container(
         // color: Colors.blue,
           margin: EdgeInsets.only(top: 40.0),
           height:MediaQuery.of(context).size.height/4,
            child: Column(children: <Widget>[
              Center(
                child: new Text(
                  "Welcome ",
                  style: TextStyle(fontSize: 40),
                ),
              ),
              Center(
                child: new Text(
                  "to ",
                  style: TextStyle(fontSize: 40),
                ),
              ),
              Center(
                child: new Text(
                  "e-Bazaar ",
                  style: TextStyle(fontSize: 40),
                ),
              ),
            ])),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: new Text("Login as a ShopKeeper",style: TextStyle(color: Colors.white),),
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                onPressed: () {
                  print("ShopLoginPage");
                  return Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => ShopLoginPage()));
                },
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: new Text("Login as a Customer",style: TextStyle(color: Colors.white)),
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                onPressed: () {
                  return Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => LoginPage()));
                },
              )
            ],
          ),
        ),
      ],
    )
    );
  }
}
