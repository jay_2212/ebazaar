import 'dart:core';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'package:shopping_cart/ui/AddtoCart.dart';

import 'TabBarView.dart';
import 'first_screen.dart';

class ProceedToBuy extends StatefulWidget {
  @override
  _ProceedToBuyState createState() => _ProceedToBuyState();
}

class _ProceedToBuyState extends State<ProceedToBuy> {
  String customer_contact_num;
  String customer_name;
  String another_contact_num;
  String customer_Address;
  String customer_email_id;

  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  final DBRef = FirebaseDatabase.instance.reference();

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Center(
            child: (Text("Shopkeeper Page", style: TextStyle(color: Colors.white))),
          )),
      body: new Center(
        child: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("customer contact num")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'customer contact num',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  customer_contact_num = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  onlyShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("customer_name")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'customer_name',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  customer_name = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("another_contact_num")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'another_contact_num',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  another_contact_num = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("customer_Address")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'customer_Address',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  customer_Address = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 30.0),
        new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[new Text("customer_email_id")],
            )),
        Column(
          children: <Widget>[
            ListTile(
              title: new TextFormField(
                cursorColor: Colors.orange,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'customer_email_id',
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                ),
                //validator: FormValidator().validateEmail,
                onChanged: (String value) {
                  customer_email_id = value;
                },
              ),
              trailing: IconButton(
                icon : Icon(Icons.add),
                onPressed: (){
                  addShop();
                },
              ),
            ),
          ],
        ),
        //new SizedBox(height: 3.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              //Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
              addShop();
            },
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text(
              'Done',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      //  new SizedBox(height: 3.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {

              Navigator.push(context, MaterialPageRoute(builder: (context) => AddtoCart()));
            },
            padding: EdgeInsets.all(12),
            color: Colors.orange,
            child: Text(
              'Internet Banking',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        //new SizedBox(height: 1.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              RecipeListPage().createState().seeadditems();
             // Navigator.push(context, MaterialPageRoute(builder: (context) => TabbarPage()));
            },
            color: Colors.orange,
            child: Text(
             "Cash On Delivery",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),

      ],
    );
  }

  void addShop(){
    DBRef.child("Customer_Confirmation").child(customer_contact_num).set({
      "customer_name" : customer_name,
      "customer_Address" : customer_Address,
      "another_contact_num" : another_contact_num,
      "customer_email_id" : customer_email_id,
    });
  }
  void onlyShop(){

  }
}