import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static Future<SharedPreferences> getPrefs() {
    return SharedPreferences.getInstance();
  }

  static Future<bool> setStringPreference(String key, String value) async {
    SharedPreferences prefs = await getPrefs();
    return await prefs.setString(key, value);
  }

  static Future<bool> setBooleanPreference(String key, bool value) async {
    SharedPreferences prefs = await getPrefs();
    return await prefs.setBool(key, value);
  }

  static Future<bool> setIntegerPreference(String key, int value) async {
    SharedPreferences prefs = await getPrefs();
    return prefs.setInt(key, value);
  }

  static Future<bool> setDoublePreference(String key, double value) async {
    SharedPreferences prefs = await getPrefs();
    return prefs.setDouble(key, value);
  }

  static void setAppToken(String token) {
    setStringPreference("token", token);
  }

  static Future<String> getAppToken() {
    return getStringPreference("token");
  }

  static void setLoggedIn(bool isLoggedIn) {
    setBooleanPreference("isLoggedIn", isLoggedIn);
  }

  static Future<bool> isLoggedIn() async {
    return getBooleanPreference("isLoggedIn");
  }


  static void setUserName(String shop_name) {
    setStringPreference("shop_name", shop_name);
  }

  static Future<String> getUserName() {
    return getStringPreference("shop_name");
  }

  static void setUser(bool issetUser) {
    setBooleanPreference("issetUser", issetUser);
  }

  static Future<bool> issetUser() async {
    return getBooleanPreference("issetUser");
  }

  static void setEligibleStatus(String is_eligible) {
    setStringPreference("is_eligible", is_eligible);
  }

  static Future<String> getEligibleStatus() {
    return getStringPreference("is_eligible");
  }

  static void setEligibleUser(bool Eligible) {
    setBooleanPreference("Eligible", Eligible);
  }

  static Future<bool> Eligible() async {
    return getBooleanPreference("Eligible");
  }



  static void setRecipe_id(String recipe_id) {
    setStringPreference("is_eligible", recipe_id);
  }

  static Future<String> getRecipe_id() {
    return getStringPreference("recipe_id");
  }

  static void setRecipeId(bool RecipeMethod) {
    setBooleanPreference("Eligible", RecipeMethod);
  }

  static Future<bool> RecipeMethod() async {
    return getBooleanPreference("RecipeMethod");
  }



  static void setmachine_connection(String machine_connection) {
    setStringPreference("machine_connection", machine_connection);
  }

  static Future<String> getmachine_connection() {
    return getStringPreference("machine_connection");
  }

  static void setMachineConnection(bool machine_connected) {
    setBooleanPreference("machine_connected", machine_connected);
  }

  static Future<bool> machine_connected() async {
    return getBooleanPreference("machine_connected");
  }

  static Future<bool> removeToken(bool token) {
    return removePreference("token");
  }

  static void setDevice(String BLEconnectedDevice) {
    setStringPreference("BLEconnectedDevice", BLEconnectedDevice);
  }

  static Future<String> getDevice() {
    return getStringPreference("BLEconnectedDevice");
  }

  static Future<bool> getBooleanPreference(String key) async {
    SharedPreferences prefs = await getPrefs();
    return prefs.containsKey(key) ? prefs.getBool(key) : false;
  }

  static Future<String> getStringPreference(String key) async {
    SharedPreferences prefs = await getPrefs();
    return prefs.containsKey(key) ? prefs.getString(key) : "";
  }

  static Future<bool> removePreference(String key) async {
    SharedPreferences prefs = await getPrefs();
    return prefs.remove(key);
  }

}
