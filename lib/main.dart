import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopping_cart/blocs/themeChanger.dart';
import 'package:shopping_cart/ui/ShopDetails.dart';
import 'package:shopping_cart/ui/first_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeChanger>(
//      builder: (_) => ThemeChanger(ThemeData.light()),
      child: MaterialAppWithTheme(), create: (BuildContext context) {},
    );
  }
}

class MaterialAppWithTheme extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeChanger>(context);
    return MaterialApp(
      title: "e-Bazaar",
      debugShowCheckedModeBanner: false,
      home: ShopFirstScreen(),
     // theme: theme.getTheme(),
    );
  }
}
